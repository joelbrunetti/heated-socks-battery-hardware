# Heated Socks Battery Hardware

Custom home built battery pack to be used with custom home built heated socks or Lenz Socks 5.0.

## Overview

Functionality:

- Output battery voltage up to 3.1 A or as low as a 1.4 Ohm load.
    - Power level will modulated using a 10 Hz output pulse with a duty cycle of 0.3 to 0.8.
- Powered by a Lithium Ion 18650 Battery of at least 2400 mAh with 3500 mAh preferred.
- Charge attached Lithium Ion Battery from USB at a rate of at least 1 A.
- Control power level with a single pushbutton. Off, Low, Medium, High
- LED Indicators:
    - Charging:
        - Green 4 Hz Blink : Powered but not charging.
        - Green 1 Hz Blink : Charging.
        - Green Solid : Charged.
    - Power Level:
        - Red : output 0.8 duty cycle.
        - Orange : output 0.65 duty cycle.
        - Yellow : output 0.3 duty cycle.
    - Capacity Left:
        - Red, Orange & Yellow blink duty cycle:
            - 100% to 75% - Solid
            - 74% to 50% - 1 Hz Blink
            - 49% to 25% - 2 Hz Blink
            - 24 % to 0% - 4 Hz Blink

## Related Documents

- [Lenz rcB 1200 & Socks 5.0 Notes](documentation/lenz_rcb1200_socks_5.0_notes.md)
- [Hardware Design Document](documentation/design_document.md)
- [Calculations](documentation/calculations.ods)
- [Layout Notes](documentation/layout_notes.md)

## Reviews

- [Schematic Checklist](documentation/schematic_checklist.md)
- [PCB Layout Checklist](documentation/pcb_layout_checklist.md)

## Firmware

- [Heated Socks Battery Firmware](http://todo)
