# Schematic Checklist

- Author: Joel Brunetti
- Date: 2021-02-19

## Procedural

- [X] Address issues from any previous review.
- [X] Update design document to match schematic.

## Mechanical

- [X] Correct board outline included.
- [X] Correct mounting pattern included.
- [X] Part height appropriate for enclosure.

## Connectors

- [X] Programming connector (JTAG) included.
- [X] Connector pinout checked against mating connectors.
- [X] Connector electrical properties match mating signals.
- [X] Ground pins shild sensitive signals and are selected with return path in mind.
- [X] Signals default to known state if mating connector missing.
- [X] External signals include ESD protection.

## Electrical

- [X] Electrical rules check (ERC) passes with no warning.
- [X] Components rated for input voltage.
- [X] Resistors have sufficient power dissipation.
- [X] Capacitors derated for voltage rail.
- [X] Bypass capacitors for each voltage rail for each IC.
- [X] Pull-up on open collector outputs.
- [X] Pull-downs on open emitter outputs.
- [X] Unused inputs have pull-up, pull-down, or no-connect as appropriate.
- [X] Proper termination for differential signals.
- [X] Output power has current limit & fault protection.
- [X] Power supply enable sequence is correct.
- [X] Power supplies appropriate for operating & in-rush current.
- [X] Reset circuit reliable.
- [X] Analogue signals are seperate from digital signals.
- [X] Analogue ground seperate from digital ground.
- [X] High power components with termal considerations are checked.
- [X] Test point values should match the desired silkscreen label.
- [X] Test points for:
    - [X] Ground
    - [X] Each power rail.
    - [X] Critical analogue signals.
    - [X] Spare pins on any microcontroller and/or FPGA.

## Layout Notes

- [X] Includes board dimensions or drawing.
- [X] Includes board mounting pattern or drawing.
- [X] Includes board properties:
    - [X] Material.
    - [X] Thickness.
    - [X] Copper weight.
- [X] Identify high current nets with desired current rating.
- [X] Identify impedance controlled nets with desired impedance.
- [X] Identify differential pairs with desired impedance.
- [X] Identify high power components which require thermal considerations.
- [X] Instruct the layout designer to include test point value on the silkscreen.
- [X] Instruct the layout designed to include other silkscreen text:
    - [X] Project name.
    - [X] Revision.

## Informational

- [X] Connectors are placed on sheet 1.
- [X] Sheets or functional blocks on a sheet have a short description of function.
- [X] Every sheet includes:
    - [X] Project name.
    - [X] Designer name.
    - [X] Revision.
    - [X] Sheet number.
    - [X] Total sheet number.
- [X] Every component includes:
    - [X] Visible reference.
    - [X] Visible part number or value for passive components.
    - [X] Visible value of DNP for unpopulated components.
    - [X] Part number.
    - [X] Footprint with pinout that matches schematic symbol pinout.
- [X] Text: 
    - [X] Does not overlap.
    - [X] Is at 0 degrees or 90 degrees only.
- [X] Any calculation completed are included or referenced (to another file) on schematic as text.

## Notes
