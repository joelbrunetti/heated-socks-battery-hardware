# PCB Layout Checklist

- Author: Joel Brunetti
- Date: 2021-02-20

## Procedural

- [X] Address issues from any previous review.
- [X] Update design document to match layout.

## Mechanical

- [X] Board dimension matches included dimenstions or drawing.
- [X] Board mounting pattern matches included pattern or drawing.
- [X] Board properties match included specifications.
- [X] Height constraints are met.
- [X] All components include 3D model.
- [X] Mounting holes are electrically isolated or grounded.
- [X] Mounting hole keep out zones are respected.
- [X] Vias not be placed in soldier pads.
- [X] Component and drawing keep out areas are respected.
- [X] High power component thermal relief is met.

## Connectors

- [X] Connector orientation matches schematic.

## Footprints

- [X] Component footprints match schematic symbol.
- [X] Polarized components have pin 1 clearly marked.

## Electrical

- [X] ESD protection placed next to connectors.
- [X] Output drivers placed next to connectors.
- [X] Bypass capacitors next to the IC power pins.
- [X] Signal termination placed next to input pins.
- [X] Analogue signals are seperate from digital signals.
- [X] Analogue ground seperate from digital ground and connected at a single point.
- [X] Sensitive nets kept away from noisy components.
- [X] Noisy nets kept away from sensitive components.
- [X] No accidental dead end traces; also known as antennas.
- [X] High current nets are of appropriate thickness for desired current rating.
- [X] High current nets include multiple vias for desired current rating.
- [X] Impedance controlled nets have desired impedance.
- [X] Differential pairs are length matched with desired impedance.
- [X] Power and ground planes are used if possible.

## Informational

- [X] Silkscreen:
    - [X] Text is at 0 degrees or 90 degrees only.
    - [X] Not over vias or soldier pads.
    - [X] Includes:
        - [X] Project name.
        - [X] Revision.
    - [X] Test points labelled with value.

## Notes
