# Hardware Design Document

Basic Systems:

- Battery
- Connectors:
    - Snaps for Output
    - USB for Charging
    - Battery Holder
    - Programming
- Battery Protection System
- Battery Charging System
- Microcontroller
- Power Control
- Pushbutton
- LED Indicators (Green, Yellow, Orange, Red)

## Battery

While bulky, Lithium Ion 18650 cells are available from multiple sources and in a range of capacities.

The Samsung 25E 18650 is a 3.6 V nominal Lithium Ion battery with a 3500 mAh capacity that will be used.

| Property                 | Value    |
| ---                      | ---      |
| Capacity                 | 3500 mAh |
| Charge Voltage           | 4.2 V    |
| Nominal Voltage          | 3.6 V    |
| Max Charge Current       | 2000 mA  |
| Max Discharge Current    | 8000 mA  |
| Discharge Cutoff Voltage | 2.65 V   |

**This battery is a raw cell and includes not protection circuitry.**

Expected discharge:

- With Socks 5.0 at 6.1 Ohms at 0.8 duty cycle, expected discharge time is ~7.4 hours.
- With Socks 5.0 at 6.1 Ohms at 0.3 duty cycle, expected discharge time is ~19.7 hours.
- With customs socks at 1.4 Ohms at 0.8 duty cycle, expected discharge time is ~1.7 hours.

**Links:**

- [Vendor](https://www.18650batterystore.com/collections/18650-batteries/products/samsung-35e#description)
- [Datasheet](https://cdn.shopify.com/s/files/1/0481/9678/0183/files/samsung_35e_data_sheet.pdf?v=1605015771)

## Connectors

### Snaps

Connects the battery hardware to the sock.

Snaps compatible with the OEM snaps may be hard to find.

Steel Snap Fasteners #633 should be compatible.
This vendor was able to provide measurements of the stud width for all of their variants.

**Links:**

- [Vendor](https://www.etsy.com/ca/listing/731998028/multi-size-silver-color-metal-snap?ref=cart&variation0=1454529110&variation1=1454529108)

### USB

Charges the battery via USB.

A right angle through hole USB C power only connector will be used.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/gct/USB4125-GF-A/13547388)
- [Datasheet](https://gct.co/files/drawings/usb4125.pdf)

### Battery

Holds the battery cell to the board.

The Keystone 1042 is a single cell 18650 holder will hold any 18650 battery.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/keystone-electronics/1042/2745668)
- [Datasheet](https://www.keyelco.com/userAssets/file/M65p27.pdf)

### Programming

There will be no dedicated programming connector.

A 2 row 10 pin 1.27 mm through hole footprint will be placed to accept a FTSH-105-04-F-D.
This is compatible with the Black Magic Prove V2.0 and included ribbon cable.

The footprint will be modified to offset alternating pins, allowing for a press fit insertion without soldering.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/samtec-inc/FTSH-105-04-F-D/6678180)
- [Datasheet](http://suddendocs.samtec.com/catalog_english/ftsh_th.pdf)

## Battery Protection System

As the battery to be used is a raw cell, proper protection must be included.

Protection must:
 - Prevent an overcharge voltage above 4.2 V.
 - Prevent an overdischarge voltage below 2.65 V.
 - Prevent a discharge rate above 2 A.
 - Prevent a short circuit.

The Diodes Inc AP9101C seems to be the only hand soldierable part that can meet this criteria.

The AP9010CK6-CQTRG1 has:
 - Overcharge detection voltage of 4.25 V.
 - Overdischarge detection voltage of 2.80 V.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/diodes-incorporated/ap9101ck6-cqtrg1/7352561)
- [Datasheet](https://www.diodes.com/assets/Datasheets/AP9101C.pdf)

### Battery Protection Switch

The APC9101C requires two external N-Channel MOSFET switches with a common drain.

These switches must:
 - Low Rdson.
 - Source > 2 A.

The Diodes Inc DMG6968UDM-7 is a 6.5A Dual N-Channel Common Drain MOSFET with an Rdson of 24 mOhm in a SOT-26 package.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/diodes-incorporated/DMG6968UDM-7/2075441)
- [Datasheet](https://www.diodes.com/assets/Datasheets/ds31758.pdf)

## Battery Charging System

The battery charing system must:
 - Charge to 4.2 V
 - Charge at a max of 1000 mA.

The MCP73833 is a stand-alone Lithium Ion charge management controller which will be used to charge the battery via 5 V with max 1 A USB input.

The PROG resistor will be selected to allow for 1000 mA charge current.

No thermistor will be included in this application.

The STAT1, STAT2, and PG outputs will be connected to the microcontroller for status information.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/microchip-technology/MCP73833-AMI-UN/1223157)
- [Datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/22005b.pdf)

## Microcontroller

A low power microcontroller with the following properties is required:

- Hand soldierable.
- Supported by Rust.
- Supported by Black Magic Probe V2.0.
- Internal oscillator.
- Two (2) inputs with interrupts:
    - One (1) for PG from the Battery Charge System.
    - One (1) for the pushbutton.
- Two (2) additional inputs.
- Five (5) outputs.
- At least two timers:
    - One for 10 Hz duty cycle power level.
    - One for indicator duty cycle.

Optional properties include:

- An ADC to measure battery voltage.
- Third timer for pushbutton debounce.

The STM32L011F3P6 is a 20TSSOP microcontroller with all of the requied and optional properties.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/stmicroelectronics/STM32L011F3P6/6166957)
- [Datasheet](https://www.st.com/resource/en/datasheet/stm32l011f3.pdf)

### Programming

This microcontroller will be programmed using Serial Wire Debug (SWD).

It will need dedicated connections for SWDIO (PA13), SWCLK (PA14), VCC & GND.

### Power Regulation

This microcontroller has in input range of 1.65 V to 3.6 V with a maximum current of 105 mA.

The battery output varies from 4.2 V to 3.7 V.

The MIC5504-3.3YM5 is a 3.3 V 300 mA Low-Dropout Voltage Regulator with a 160 mV drop.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/microchip-technology/MIC5504-3-3YM5-TR/4864018)
- [Datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/MIC5501-02-03-04-300mA-Single-Output-LDO-in-Small-Packages-DS20006006B.pdf)

## Power Control

A switch capable of of switching the battery power (4.2 to 3.7 V at a maximum of 1500 mA) at 10 Hz with a low resistance is needed.

The SIP32510DT-T1-GE3 is a 1.2V to 5.5V 3A Load Switch with a 44 mOhm Rds and an Ven high voltage as low as 1.4 V.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/vishay-siliconix/SIP32510DT-T1-GE3/4496335)
- [Datasheet](https://www.vishay.com/docs/63577/sip32510.pdf)

## Pushbutton

A nice tactile pushbutton for selecting the power level is needed. This should be surface mount and sealed in case of potting.

**Links:**

- [Vendor](https://www.digikey.ca/en/products/detail/c-k/KSEK43GLFS/3861159)
- [Datasheet](https://www.ckswitches.com/media/1458/kse.pdf)

## LED Indicators

Four (4) low power surface mount LEDs are required in the following colours: Green, Yellow, Orange, Red.
