# Lenz rcB 1200 & Socks 5.0 Notes

## Battery Labels

- Voltage: 3.7 V
- Capacity: 2400 mAh
- Capacity: 8.8 Wh

Each pack contains two batteries with the total capacity listed above.

## Socks Labels

- Power: 1.69 W

## Socks Measured

Measured between the outer two snaps. The middle snap does not appear to be connected.

- Resistance: 6.00 to 6.28 Ohms

## Measured Output

Measured on the battery pack with the power button facing up and the text oriented for reading:

- Ground: Leftmost snap.
- Vcc: Rightmost snap.

Battery output measured under load.

| Level   | Frequency (Hz) | Minimum Voltage (V) | Maximum Voltage (V) | Duty Cycle |
| ---     | ---            | ---                 | ---                 | ---        |
| Power 1 | 9.9            | 0                   | 4.01                | 0.3        |
| Power 2 | 9.9            | 0                   | 3.99                | 0.56       |
| Power 3 | 9.9            | 0                   | 3.95                | 0.8        |
| App 1   | 9.9            | 0                   | 3.95                | 0.3        |
| App 5   | 9.9            | 0                   | 3.95                | 0.64       |
| App 9   | 9.9            | 0                   | 3.95                | 0.8        |

## Fasteners

Snap fastners are used to connect the battery pack to the sock.

### Socks

Socket with:

- Outer Diameter: 12.0 mm
- Inner Diameter: 4.5 mm
- Linear between rails: 3.5 mm

### Batteries

Stud with:

- Outer Diameter: 10 mm
- Stud Diameter: 4.5 mm
